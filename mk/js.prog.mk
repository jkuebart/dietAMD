# Copyright 2017, Joachim Kuebart <joachim.kuebart@gmail.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#	notice, this list of conditions and the following disclaimer.
#
#    2. Redistributions in binary form must reproduce the above copyright
#	notice, this list of conditions and the following disclaimer in the
#	documentation and/or other materials provided with the
#	distribution.
#
#    3. Neither the name of the copyright holder nor the names of its
#	contributors may be used to endorse or promote products derived
#	from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

SRCS		?=	${PROG_JS:.js=-src.js}
ENTRY		?=	${PROG_JS:.js=-src.js}
MAP		?=	${PROG_JS:=.map}

CLEANFILES	+=	${PROG_JS} ${MAP}

JSOWN		?=	${SHAREOWN}
JSGRP		?=	${SHAREGRP}
JSMODE		?=	${SHAREMODE}

JSCOMP		?=	UGLIFYJS

UGLIFYJS	?=	uglifyjs
UGLIFYJSFLAGS	?=	-c --enclose -m -v

NODE_MODULES	?=	${.CURDIR}/../../node_modules
GCC		?=	java -jar ${NODE_MODULES}/google-closure-compiler/compiler.jar
.if empty(GCCFLAGS)
GCCFLAGS	+=	-O ADVANCED -W VERBOSE
GCCFLAGS	+=	--language_in ECMASCRIPT6_STRICT
GCCFLAGS	+=	--language_out ECMASCRIPT5_STRICT
GCCFLAGS	+=	--new_type_inf true
GCCFLAGS	+=	--assume_function_wrapper true
GCCFLAGS	+=	--output_wrapper '(function(){%output%}.call(this));'
GCCFLAGS	+=	--dependency_mode STRICT
GCCFLAGS	+=	--rewrite_polyfills false
GCCFLAGS	+=	--use_types_for_optimization true
.endif

ROLLUP		?=	rollup
ROLLUPFORMAT	?=	es
ROLLUPFLAGS	+=	${EXTERNAL:%=-e %}
ROLLUPFLAGS	+=	${ROLLUPFORMAT:%=-f %}
ROLLUPFLAGS	+=	${ROLLUPCONFIG:%=-c %}

all:		${PROG_JS} ${MAP}

.if ${JSCOMP} == GCC
# GCC can't include source code in map files.
realinstall:	${SRCS}
.endif

realinstall:	${PROG_JS} ${MAP}
	@if ! test -d ${DESTDIR}${JSDIR}; then \
		mkdir -p ${DESTDIR}${JSDIR}; \
		if ! test -d ${DESTDIR}${JSDIR}; then \
			${ECHO} "Unable to create ${DESTDIR}${JSDIR}."; \
			exit 1; \
		fi; \
	fi
	${INSTALL} -o ${JSOWN} -g ${JSGRP} -m ${JSMODE} \
		${.ALLSRC} ${DESTDIR}${JSDIR}

UGLIFYJS_${PROG_JS}: .USE ${SRCS}
	pref=${.ALLSRC:H:C,[^/]*,,g:Q}; \
	pref=$${pref%% *}; \
	${UGLIFYJS} ${UGLIFYJSFLAGS} \
		-o ${PROG_JS} \
		--source-map-include-sources --source-map ${MAP} \
		-p $${#pref} \
		-- ${.ALLSRC}

GCC_${PROG_JS}: .USE ${SRCS} ${GCCEXTERNS}
	cd ${.CURDIR} && \
	${GCC} ${GCCFLAGS} \
		--js_output_file ${.OBJDIR}/${PROG_JS} \
		--create_source_map ${.OBJDIR}/${MAP} \
		--source_map_location_mapping ${.OBJDIR}/\| \
		${GCCEXTERNS:%=--externs %} \
		${ENTRY:%=--entry_point %} \
		${SRCS:%=--js %}
	printf '//# sourceMappingURL=%s' ${MAP} >> ${PROG_JS}

ROLLUP_${PROG_JS}: .USE ${SRCS} ${ROLLUPCONFIG}
	${ROLLUP} ${ROLLUPFLAGS} \
		-o ${PROG_JS} \
		-m true \
		${ENTRY:%=-i ${.CURDIR}/%}

${PROG_JS}:	${JSCOMP}_${PROG_JS}

${MAP}:		${PROG_JS}

.include <bsd.obj.mk>
