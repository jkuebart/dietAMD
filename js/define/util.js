/*
 * Copyright 2017, Joachim Kuebart <joachim.kuebart@gmail.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright
 *	notice, this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright
 *	notice, this list of conditions and the following disclaimer in the
 *	documentation and/or other materials provided with the
 *	distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its
 *	contributors may be used to endorse or promote products derived
 *	from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Construct an Error from the arguments.
 * @param {...string} msgs
 */
export function err(msgs) {
    return new Error([].join.call(arguments, ': '));
}

// A version of lastIndexOf that returns length if not found.
export function lastIndexOf(s, t) {
    return (1 + s.length + s.lastIndexOf(t)) % (1 + s.length);
}

// The prefix of a module ID.
export function idPrefix(s) {
    return s.slice(0, 1 + s.lastIndexOf('/'));
}

// Return the suffix of a module ID.
export function idSuffix(s) {
    return s.slice(1 + s.lastIndexOf('/'));
}

// Polyfill for Object.assign because it's missing on many platforms.
export const assign = Object.assign || function (target) {
    for (let i = 1; i < arguments.length; ++i) {
	let a = arguments[i];
	if (null != a) {
	    Object.keys(a).forEach(k => target[k] = a[k]);
	}
    }
    return target;
};

// Polyfill for String.prototype.startsWith which isn't common
export function startsWith(str, pref) {
    return str.slice(0, pref.length) === pref;
}
