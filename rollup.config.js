import buble from 'rollup-plugin-buble';
import uglify from 'rollup-plugin-uglify';

export default {
    external: [ 'text' ],
    paths: { text: './text' },
    plugins: [ buble(), uglify() ],
};
